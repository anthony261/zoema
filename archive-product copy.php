<?php

/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

get_header('shop');
?>

<?php
$argg = array(
  'taxonomy'     => 'product_cat',
  'hide_empty' => 0,
  'title_li'     => '',
);
$all_cat = get_categories($argg);
$categoriaID = $_GET['category'];
foreach ($all_cat as $cat) {
  if ($cat->cat_ID == $categoriaID) {
    $categoriaNome = $cat->cat_name;
  }
}
if ($categoriaID == NULL) {
  $categoriaNome = 'SHOP ONLINE';
}
?>

<div class="categoria">
  <div class="container">
    <div class="parallelogram">
      <div class="categoria-title">
        <span>ZOEMA <?= $categoriaNome ?></span>
      </div>
      <!-- <span>ZOEMA SHOP</span>	 -->
    </div>
    <div class="new-in-dress d-flex" style="justify-content: space-between;">
      <a href="http://localhost/Zoema/wordpress/negozio/?categoria%5B%5D=Accessori">
        <img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/accessori.png" alt="">
        <img class="img-hover" src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Ellipse-2.png" alt="">
        <span class="hoover">Accessori</span>
      </a>
      <a href="http://localhost/Zoema/wordpress/negozio/?categoria%5B%5D=T-shirt">
        <img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/t-shirt.png" alt="">
        <img class="img-hover" src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Ellipse-2.png" alt="">
        <span class="hoover" style="left: 23px;">T-shirt</span>
      </a>
      <a href="http://localhost/Zoema/wordpress/negozio/?categoria%5B%5D=Camicie">
        <img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/camicie.png" alt="">
        <img class="img-hover" src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Ellipse-2.png" alt="">
        <span class="hoover" style="left: 17px;">Camicie</span>
      </a>
      <a href="http://localhost/Zoema/wordpress/negozio/?categoria%5B%5D=Giacche">
        <img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/giacche.png" alt="">
        <img class="img-hover" src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Ellipse-2.png" alt="">
        <span class="hoover" style="left: 18px;">Giacche</span>
      </a>
      <a href="http://localhost/Zoema/wordpress/negozio/?categoria%5B%5D=Jeans">
        <img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/jeans.png" alt="">
        <img class="img-hover" src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Ellipse-2.png" alt="">
        <span class="hoover" style="left: 23px;">Jeans</span>
      </a>
      <a href="http://localhost/Zoema/wordpress/negozio/?categoria%5B%5D=Maglioni">
        <img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/maglioni.png" alt="">
        <img class="img-hover" src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Ellipse-2.png" alt="">
        <span class="hoover" style="left: 17px;">Maglioni</span>
      </a>
      <a href="http://localhost/Zoema/wordpress/negozio/?categoria%5B%5D=Pantaloni">
        <img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/pamtaloni.png" alt="">
        <img class="img-hover" src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Ellipse-2.png" alt="">
        <span class="hoover">Pantaloni</span>
      </a>
      <a href="http://localhost/Zoema/wordpress/negozio/?categoria%5B%5D=Scarpe">
        <img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/scarpe.png" alt="">
        <img class="img-hover" src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Ellipse-2.png" alt="">
        <span class="hoover" style="left: 21px;">Scarpe</span>
      </a>
      <a href="http://localhost/Zoema/wordpress/negozio/?categoria%5B%5D=Sportwear">
        <img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/sportwaer.png" alt="">
        <img class="img-hover" src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Ellipse-2.png" alt="">
        <span class="hoover" style="left: 10px;">Sportwear</span>
      </a>
      <a href="http://localhost/Zoema/wordpress/negozio/?categoria%5B%5D=Shorts">
        <img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/shorts.png" alt="">
        <img class="img-hover" src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Ellipse-2.png" alt="">
        <span class="hoover" style="left: 21px;">Shorts</span>
      </a>
    </div>
  </div>
</div>
<div class="container container-shop">
  <form role="search" class="form-search" method="get" action="<?php echo get_pagenum_link(1); ?>">
    <h1>Taglie</h1>
    <?php
    $taxonomy = get_terms('taglia', array('hide_empty' => 0));
    $size = $_GET['taglia'];
    foreach ($taxonomy as $cat) :
      $arraysizes[] = $cat->name;
    ?>
      <input onChange="this.form.submit()" type="checkbox" id="<?= $cat->name ?>" name="taglia[]" value="<?= $cat->name ?>" <?php if (isset($_GET['taglia']) && in_array($cat->name, $_GET['taglia'])) echo 'checked' ?>>
      <label for="taglia"> <?= $cat->name ?> </label><br>
    <?php
    endforeach;
    ?>
    <h1>Categorie</h1>
    <?php
    $taxonomy = 'product_cat';
    $orderby = 'name';
    $show_count = 0; // 1 for yes, 0 for no
    $pad_counts = 0; // 1 for yes, 0 for no
    $hierarchical = 1; // 1 for yes, 0 for no
    $title = '';
    $empty = 0;
    $thumbnail = 1;

    $args = array(
      'taxonomy' => $taxonomy,
      'orderby' => $orderby,
      'show_count' => $show_count,
      'pad_counts' => $pad_counts,
      'hierarchical' => $hierarchical,
      'title_li' => $title,
      'hide_empty' => $empty,
      'Thumbnail' => $thumbnail
    );
    $all_cat = get_categories($args);
    $categoryselect = $_GET['categoria'];
    $arraycaturl[] = $_GET['categoria'];

    foreach ($all_cat as $cat) :
      $arraycats[] = $cat->name;
    ?>
      <input onChange="this.form.submit()" type="checkbox" id="<?= $cat->name ?>" name="categoria[]" value="<?= $cat->name ?>" <?php if (isset($_GET['categoria']) && in_array($cat->name, $_GET['categoria'])) echo 'checked'
                                                                                                                                ?>>
      <label for="categoria"> <?= $cat->name ?> </label><br>

    <?php
    endforeach;
    ?>



    <h1>Gender</h1>
    <?php
    $arraycats = [];

    foreach ($all_cat as $cat) :
      if ($cat->name == 'Donna' || $cat->name == 'Uomo' || $cat->name == 'No Gender') {
        $arraycats[] = $cat->name;
      }

    ?>
    <?php
    endforeach;
    ?>
    <?php for ($i = 0; $i < 3; $i++) { ?>
      <input onChange="this.form.submit()" type="checkbox" id="<?= $arraycats[$i] ?>" name="categoria[]" value="<?= $arraycats[$i] ?>" <?php //if (isset($_GET['categoria']) && in_array($cat->name, $_GET['categoria'])) echo 'checked'
                                                                                                                                        ?>>
      <label for="categoria"> <?= $arraycats[$i] ?> </label><br>
    <?php } ?>

    <h1>Stagioni</h1>
    <?php
    $taxonomy = get_terms('stagione', array('hide_empty' => 0));
    $stagione = $_GET['stagione'];
    foreach ($taxonomy as $cat) :
      $arraystagioni[] = $cat->name;
    ?>
      <input onChange="this.form.submit()" type="checkbox" id="<?= $cat->name ?>" name="stagione[]" value="<?= $cat->name ?>" <?php if (isset($_GET['stagione']) && in_array($cat->name, $_GET['stagione'])) echo 'checked'
                                                                                                                              ?>>
      <label for="stagione"> <?= $cat->name ?> </label><br>
    <?php
    endforeach;
    ?>

    <h1>Prezzo</h1>
    <?php
    $arrayprice = ['0|10', '10|50', '50|100', 'ricco'];
    $price = $_GET['prezzo'];
    ?>
    <input onChange="this.form.submit()" type="checkbox" id="prezzo" name="prezzo[]" value="0|10" <?php //if (in_array($arrayprice[0], $_GET['prezzo'])) echo 'checked' 
                                                                                                  ?>>
    <label for="prezzo">Fino a 10€</label><br>
    <input onChange="this.form.submit()" type="checkbox" id="prezzo" name="prezzo[]" value="10|50" <?php //if (in_array($arrayprice[1], $_GET['prezzo'])) echo 'checked' 
                                                                                                    ?>>
    <label for="prezzo">Da 10€ a 50€</label><br>
    <input onChange="this.form.submit()" type="checkbox" id="prezzo" name="prezzo[]" value="50|100" <?php //if (in_array($arrayprice[2], $_GET['prezzo'])) echo 'checked' 
                                                                                                    ?>>
    <label for="prezzo">Da 50€ a 100€</label><br>
    <input onChange="this.form.submit()" type="checkbox" id="prezzo" name="prezzo[]" value="ricco" <?php //if (in_array($$arrayprice[3], $_GET['prezzo'])) echo 'checked' 
                                                                                                    ?>>
    <label for="prezzo">Mi sento ricco</label><br>
  </form>


  <?php
  if (woocommerce_product_loop()) {

    /**
     * Hook: woocommerce_before_shop_loop.
     *
     * @hooked woocommerce_output_all_notices - 10
     * @hooked woocommerce_result_count - 20
     * @hooked woocommerce_catalog_ordering - 30
     */
    do_action('woocommerce_before_shop_loop');
    if ($taglie == NULL) {
      $taglie = $arraysizes;
    }
    if ($categoryselect == NULL) {
      $categoryselect = $arraycats;
    }
    if ($stagione == NULL) {
      $stagione = $arraystagioni;
    }

    $query = new WC_Product_Query(array(
      'limit' => -1,
      'status' => 'publish',
      'price_range' => $price[0],
      'tax_query' => array(
        'relationshiop' => 'OR',
        array(
          'taxonomy' => 'taglia',
          'field' => 'slug',
          'terms' => $taglie,
        ),
        array(
          'taxonomy' => 'stagione',
          'field' => 'slug',
          'terms' => $stagione,
        ),
        array(
          'taxonomy' => 'product_cat',
          'field' => 'slug',
          'terms' => $categoryselect,
        )
      ),
    ));

    $products = $query->get_products();
  ?>

    <ul class="products columns-4">
      <?php
      foreach ($products as $product) {
      ?>
        <?php wc_get_template_part('content', 'product');
        ?>


      <?php
      } ?>
    </ul>



  <?php
    /**
     * Hook: woocommerce_after_shop_loop.
     *
     * @hooked woocommerce_pagination - 10
     */
    do_action('woocommerce_after_shop_loop');
  } else {
    /**
     * Hook: woocommerce_no_products_found.
     *
     * @hooked wc_no_products_found - 10
     */
  ?>

  <?php
    do_action('woocommerce_no_products_found');
  }
  ?>

</div>


<?php
get_footer('shop');
?>