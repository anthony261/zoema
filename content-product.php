<?php

/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
  return;
}
?>
<li class="card-simple card-simple-product" <?php wc_product_class('', $product); ?>>
  <?php
  $reg_price = $product->get_regular_price();
  $sale_price = $product->get_sale_price();
  ?>
  <div class="product-photo">
    <?= woocommerce_get_product_thumbnail(); ?>
  </div>
  <div class="card-desc">
    <h6 class="card-title"><?= $product->get_name(); ?></h6>
    <p class="card-brand"><?= $product->get_description(); ?></p>
    <p class="card-price"><?= $reg_price ?> €</p>
    <a class="btn-ab" href="<?= get_permalink($product->id) ?>">ACQUISTA</a>
    <div class="taglia">
      <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/Tag-promo.png'); ?>" alt="" style="display: inline-block;"> M
    </div>
  </div>

</li>