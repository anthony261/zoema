<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Stormind_Games
 */

global $product;

get_header(); ?>


<div class="container container-product-detail">
	<?php
	$reg_price = $product->get_regular_price();
	$sale_price = $product->get_sale_price();
	?>
	<div class="product-photo-detail">
		<?= woocommerce_get_product_thumbnail(); ?>
	</div>
	<div class="product-description-detail">
		<h1 class="product-description-title"><?php the_title() ?></h1>
		<h3 class="product-description-brand"><?php the_content(); ?></h3>
		<h3 class="product-description-price"><?= $reg_price ?></h3>
		<h3 class="product-description-tail">Taglia S</h3>

		<form class="cart" action="<?php echo esc_url(apply_filters('woocommerce_add_to_cart_form_action', $product->get_permalink())); ?>" method="post" enctype="multipart/form-data">

			<button type="submit" name="add-to-cart" value="<?php echo esc_attr($product->get_id()); ?>" class="single_add_to_cart_button button alt">ACQUISTA</button>

		</form>


		<div class="under-desc d-flex">
			<div class="under-desc-icon">
				<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/descrizione.png'); ?>" alt="">
				<h4>DESCRIZIONE</h4>
			</div>
			<div class="under-desc-icon">
				<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/dettagli.png'); ?>" alt="">
				<h4>DETTAGLI</h4>
			</div>
			<div class="under-desc-icon">
				<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/consegna.png'); ?>" alt="">
				<h4>CONSEGNA</h4>
			</div>

		</div>
		<p class="under-desc-detail">Offriamo spedizioni gratuite per ordini sopra i 60€ in Italia, sopra i 100€ in Europa e sopra i 120€ nel resto del mondo. Al di sotto di questi limiti i costi variano in funzione della destinazione: 5.90€ per consegne in Italia, 8.90€ in Europa occidentale, 10.90€ nel resto d'Europa, 21.90€ negli USA e 31.90€ negli altri paesi. Per ridurre l'impatto ambientale delle nostre spedizioni, usiamo imballi prodotti con materiali riciclati al 100% e concepiti per essere riutilizzati in caso di reso. Accettiamo senza problemi resi entro 30 giorni. Rimborsiamo i costi di spedizione dei resi solo se i capi restituiti hanno difetti grossolani ed evidenti che non erano visibili nelle foto e/o menzionati nella descrizione del prodotto. Nel caso di restituizioni da paesi al di fuori dell'Unione Europea, rimborsiamo l'importo della merce acquistata al netto di eventuali spese doganali.</p>
	</div>
</div>

<div class="container container-outfit">
	<div class="outfit">
		<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Group-242.png" alt="" style="display: inline-block;"> COMPLETA L’OUTFIT
	</div>

	<div class="row">
		<?php
		$args = array(
			'post_type' => 'product',
			'posts_per_page'  => 4
		);
		$prodotto = new WP_Query($args);


		while ($prodotto->have_posts()) : $prodotto->the_post();
		?>

			<?php $url_prodotto = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
			$url_prodotto = $url_prodotto[0] ?>
			<div class="card-simple col-3">
				<?php
				$reg_price = $product->get_regular_price();
				$sale_price = $product->get_sale_price();
				?>
				<div class="card-photo">
					<img src="<?php echo $url_prodotto ?>" alt="">
				</div>
				<div class="card-desc">
					<h6 class="card-title"><?php the_title(); ?></h6>
					<p class="card-brand"><?php the_content(); ?></p>
					<p class="card-price"> <?= $reg_price ?> €</p>







					<button class="btn-ab">ACQUISTA</button>
					<div class="taglia">
						<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Tag.png" alt="" style="display: inline-block;"> S
					</div>
				</div>
			</div>

		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</div>
</div>





<?php
get_footer();
?>