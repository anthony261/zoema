<?php

/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>


<?php do_action('storefront_before_footer'); ?>

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="col-full">
		<div class="container">
			<div class="left-footer">
				<div class="footer-title">
					<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Vector-17.png" alt="" style="display: inline-block;">
					Join in the Vintage iconic style
				</div>
				<div class="footer-subtitle">
					Iscriviti alla NEWSLETTER per ricevere sconti, offerte e news!
				</div>
				<input type="text" placeholder="Type here" class="footer-input">
				<input type="checkbox">
				Semper feugiat nibh sed pulvinar
				<div class="last-section">
					<div class="language">
						<h2>LANGUAGE</h2>
						<span class="ita">
							<a href="">ITA</a> |
						</span>
						<span class="eng"><a href="">EN</a></span>
					</div>
					<div class="follow-zoema">
						<div class="title">
							<h2>FOLLOW ZOEMA</h2>
						</div>
						<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Vector-18.png" alt="" style="display: inline-block;">
						<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Group-2.png" alt="" style="display: inline-block;">
					</div>
					<div class="philosophy">
						<h2>ZOEMA PHILOSOPHY</h2>
						<button class="btn-ab">LEGGI</button>
					</div>
				</div>
			</div>
			<div class="right-footer">
				<div class="right-footer-icon">
					<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Vector-20.png" alt="">
					<h4>contact us</h4>
				</div>
				<div class="right-footer-icon">
					<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Vector-21.png" alt="">
					<h4>resi</h4>
				</div>
				<div class="right-footer-icon" style="margin-right: 0;">
					<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Group-5.png" alt="">
					<h4>pagamenti sicuri</h4>
				</div>
				<div class="right-footer-icon">
					<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Vector-19.png" alt="">
					<h4>consegna gratuita</h4>
				</div>
				<div class="right-footer-icon">
					<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Group-3.png" alt="">
					<h4>spedizioni</h4>
				</div>
				<div class="right-footer-icon" style="margin-right: 0;">
					<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Group-4.png" alt="">
					<h4>cookie policy</h4>
				</div>
			</div>
		</div>
		<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Frame-42.png" alt="" width="100%">
		<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/powered-desk.png" alt="" width="100%">


	</div><!-- .col-full -->
</footer><!-- #colophon -->

<?php do_action('storefront_after_footer'); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>