<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Zoema
 */

get_header();
?>

<main id="primary" class="site-main">


  <div class="hero">
    <div class="hero-message">
      Non è passato di moda, è moda passata
    </div>
    <div class="hero-buttons">
      <a class="btn-ab" href="<?php echo get_permalink(1) . '/negozio/?categoria=Donna' ?>">
        DONNA
      </a>
      <a class="btn-ab" href="<?php echo get_permalink(1) . '/negozio/?categoria=No-Gender' ?>" style="margin-left: 33px; margin-right: 33px;">
        NO GENDER
      </a>
      <a class="btn-ab" href="<?php echo get_permalink(1) . '/negozio/?categoria=Uomo' ?>">
        UOMO
      </a>

    </div>
  </div>


  <div class="under-hero row">
    <div class="col-3">
      <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/secure-payment.png'); ?>" alt="">
      <p>Visa, Mastercard, Amex</p>
    </div>
    <div class="col-3">
      <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/free-delivery.png'); ?>" alt="">
      <p style="padding-left: 15px;">per ordini over 60 €</p>
    </div>
    <div class="col-3">
      <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/free-delivery.png'); ?>" alt="">
      <p style="padding-left: 33px;">in tutta Italia</p>
    </div>
    <div class="col-3">
      <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/contact-us.png'); ?>" alt="">
      <p>Email, Whatsapp, Direct</p>
    </div>
  </div>


  <div class="promo">
    <div class="promo-title container" style="padding-left: 0 !important; padding-right: 0 !important; margin-bottom: 48px;">
      <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/i.png'); ?>" alt="" style="display: inline-block;">
      IN PROMO
    </div>
    <div class="container row" style="padding-left: 0 !important; padding-right: 0 !important;">
      <?php
      $args = array(
        'post_type' => 'product',
        'posts_per_page'  => 4
      );
      $prodotto = new WP_Query($args);


      while ($prodotto->have_posts()) : $prodotto->the_post();
      ?>

        <?php $url_prodotto = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
        $url_prodotto = $url_prodotto[0] ?>

        <div class="card-simple col-3">
          <?php
          $reg_price = $product->get_regular_price();
          $sale_price = $product->get_sale_price();
          ?>
          <img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Group-12.png" alt="" style="position: absolute; top: 12px; left: 12px;">
          <div class="card-photo">
            <img src="<?php echo $url_prodotto ?>" alt="">
          </div>
          <div class="card-desc">
            <h6 class="card-title"><?php the_title(); ?></h6>
            <p class="card-brand"><?php the_content(); ?></p>
            <p class="card-price"> <?= $reg_price ?> €</p>
            <button class="btn-ab">ACQUISTA</button>
            <div class="taglia">
              <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/Tag-promo.png'); ?>" alt="" style="display: inline-block;"> S
            </div>
          </div>
        </div>
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
    </div>
  </div>


  <div class="saldi">
    <div class="container d-flex">
      <div class="saldi-card" style="background-image: url('<?= esc_url(get_stylesheet_directory_uri() . '/img/camicia-donna.png') ?>');">
        <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/promo.png') ?>" alt="">
        <a class="btn-ab" href="<?php echo get_permalink(1) . '/negozio/?categoria=No-Gender' ?>">NO GENDER</a>
      </div>
      <div class="saldi-card" style="background-image: url('<?= esc_url(get_stylesheet_directory_uri() . '/img/camicia-donna.png') ?>'); margin-left: 18px; margin-right: 18px;">
        <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/promo.png') ?>" alt="">
        <a class="btn-ab" href="<?php echo get_permalink(1) . '/negozio/?categoria=Uomo' ?>">UOMO</a>
      </div>
      <div class="saldi-card" style="background-image: url('<?= esc_url(get_stylesheet_directory_uri() . '/img/camicia-donna.png') ?>');">
        <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/promo.png') ?>" alt="">
        <a class="btn-ab" href="<?php echo get_permalink(1) . '/negozio/?categoria=Donna'?>">DONNA</a>
      </div>
    </div>
    <h1>È ORA DEI SALDI</h1>
    <?php
    $shop_page_url = get_permalink(wc_get_page_id('shop')); ?>
    <a class="btn-ab acq" href="<?= $shop_page_url ?>">ACQUISTA</a>
  </div>


  <div class="new-in">
    <div class="container">
      <div class="new-in-title">
        <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/i.png'); ?>" alt="" style="display: inline-block;">
        NEW IN
      </div>
      <div class="new-in-dress mb-5">
        <a href="<?php echo get_permalink(1) . '/negozio/?categoria=Accessori' ?>">
          <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/camicie.png'); ?>" alt="">
          <img class="img-hover" src="<?= esc_url(get_stylesheet_directory_uri() . '/img/Ellipse-2.png'); ?>" alt="">
          <span class="hoover">Accessori</span>
        </a>
        <a href="<?php echo get_permalink(1) . '/negozio/?categoria=T-shirt' ?>">
          <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/camicie.png'); ?>" alt="">
          <img class="img-hover" src="<?= esc_url(get_stylesheet_directory_uri() . '/img/Ellipse-2.png'); ?>" alt="">
          <span class="hoover" style="left: 23px;">T-shirt</span>
        </a>
        <a href="<?php echo get_permalink(1) . '/negozio/?categoria=Camicie' ?>">
          <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/camicie.png'); ?>" alt="">
          <img class="img-hover" src="<?= esc_url(get_stylesheet_directory_uri() . '/img/Ellipse-2.png'); ?>" alt="">
          <span class="hoover" style="left: 17px;">Camicie</span>
        </a>
        <a href="<?php echo get_permalink(1) . '/negozio/?categoria=Giacche' ?>">
          <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/camicie.png'); ?>" alt="">
          <img class="img-hover" src="<?= esc_url(get_stylesheet_directory_uri() . '/img/Ellipse-2.png'); ?>" alt="">
          <span class="hoover" style="left: 18px;">Giacche</span>
        </a>
        <a href="<?php echo get_permalink(1) . '/negozio/?categoria=Jeans' ?>">
          <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/camicie.png'); ?>" alt="">
          <img class="img-hover" src="<?= esc_url(get_stylesheet_directory_uri() . '/img/Ellipse-2.png'); ?>" alt="">
          <span class="hoover" style="left: 23px;">Jeans</span>
        </a>
        <a href="<?php echo get_permalink(1) . '/negozio/?categoria=Maglioni' ?>">
          <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/camicie.png'); ?>" alt="">
          <img class="img-hover" src="<?= esc_url(get_stylesheet_directory_uri() . '/img/Ellipse-2.png'); ?>" alt="">
          <span class="hoover" style="left: 17px;">Maglioni</span>
        </a>
        <a href="<?php echo get_permalink(1) . '/negozio/?categoria=Pantaloni' ?>">
          <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/camicie.png'); ?>" alt="">
          <img class="img-hover" src="<?= esc_url(get_stylesheet_directory_uri() . '/img/Ellipse-2.png'); ?>" alt="">
          <span class="hoover">Pantaloni</span>
        </a>
        <a href="<?php echo get_permalink(1) . '/negozio/?categoria=Scarpe' ?>">
          <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/camicie.png'); ?>" alt="">
          <img class="img-hover" src="<?= esc_url(get_stylesheet_directory_uri() . '/img/Ellipse-2.png'); ?>" alt="">
          <span class="hoover" style="left: 21px;">Scarpe</span>
        </a>
        <a href="<?php echo get_permalink(1) . '/negozio/?categoria=Sportwear' ?>">
          <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/camicie.png'); ?>" alt="">
          <img class="img-hover" src="<?= esc_url(get_stylesheet_directory_uri() . '/img/Ellipse-2.png'); ?>" alt="">
          <span class="hoover" style="left: 10px;">Sportwear</span>
        </a>
        <a href="<?php echo get_permalink(1) . '/negozio/?categoria=Shorts' ?>">
          <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/camicie.png'); ?>" alt="">
          <img class="img-hover" src="<?= esc_url(get_stylesheet_directory_uri() . '/img/Ellipse-2.png'); ?>" alt="">
          <span class="hoover" style="left: 21px;">Shorts</span>
        </a>
      </div>
    </div>
    <div class="new-in-card row container">
      <?php
      $args = array(
        'post_type' => 'product',
        'posts_per_page'  => 8
      );
      $prodotto = new WP_Query($args);


      while ($prodotto->have_posts()) : $prodotto->the_post();
      ?>

        <?php $url_prodotto = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
        $url_prodotto = $url_prodotto[0] ?>
        <div class="card-simple col-3">
          <?php
          $reg_price = $product->get_regular_price();
          $sale_price = $product->get_sale_price();
          ?>
          <div class="card-photo">
            <img src="<?php echo $url_prodotto ?>" alt="">
          </div>
          <div class="card-desc">
            <h6 class="card-title"><?php the_title(); ?></h6>
            <p class="card-brand"><?php the_content(); ?></p>
            <p class="card-price"> <?= $reg_price ?> €</p>
            <a class="btn-ab" href="" style="display: inline-block;">ACQUISTA</a>
            <div class="taglia">
              <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/Tag-promo.png'); ?>" alt="" style="display: inline-block;"> S
            </div>
          </div>
        </div>
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>

    </div>
    <?php
    $shop_page_url = get_permalink(wc_get_page_id('shop')); ?>
    <button class="btn-ab"><a href="<?= $shop_page_url ?>" style="text-decoration: none; color: #2D2D2D">SHOP</a>
    </button>
  </div>


  <div class="follow">
    <div class="container">
      <div class="follow-title">
        <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/i.png'); ?>" alt="" style="display: inline-block;"> FOLLOW US ON INSTAGRAM
      </div>
      <p>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p>

      <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/jeans-donna.png'); ?>" alt="" style="display: inline-block;" class="follow-img">
      <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/jeans-donna.png'); ?>" alt="" style="display: inline-block;" class="follow-img">
      <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/jeans-donna.png'); ?>" alt="" style="display: inline-block;" class="follow-img">
      <img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/jeans-donna.png'); ?>" alt="" style="display: inline-block;" class="follow-img">
      <button class="btn-ab">FOLLOW!</button>
    </div>
  </div>


</main>



<?php
get_footer();
?>