<?php

function childtheme_parent_styles()
{
  wp_enqueue_style('parent', get_stylesheet_directory_uri() . "/style.css", array());
  wp_enqueue_style('test', get_stylesheet_directory_uri() . "/style/main.css", array('parent'));
}

add_action('wp_enqueue_scripts', 'childtheme_parent_styles');


/* Bootstrap */
add_action('wp_enqueue_scripts', 'script_custom', 10);
function script_custom()
{

  wp_register_script('tether',  'https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"', '', '', true);
  wp_enqueue_script('tether');

  wp_register_script('popper',  'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', '', '', true);
  wp_enqueue_script('popper');

  wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js', '', '', true);
  wp_enqueue_script('bootstrap');
}


add_action('wp_footer', 'wpdocs_scripts_method');


function wpdocs_scripts_method()
{
  wp_register_script('checkout_ajax_script2', get_stylesheet_directory_uri() . '/js/multistep.js', [], NULL, false);
  wp_enqueue_script('checkout_ajax_script2');
  wp_localize_script('checkout_ajax_script2', 'showData', []);



  wp_register_script('payment-storage-checkout', get_stylesheet_directory_uri() . '/js/payment-storage.js', [], NULL, false);
  wp_enqueue_script('payment-storage-checkout');
  wp_localize_script('payment-storage-checkout', 'storeData', []);
}



add_action('wp_enqueue_scripts', 'custom_style');
function custom_style(){
  wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css');
}

register_nav_menus(
  array(
    'menu-1' => esc_html__('Primary', 'zoema'),
    'menu-2' => esc_html__('secondary', 'zoema')
  )
);

add_filter('woocommerce_product_data_store_cpt_get_products_query', 'handle_price_range_query_var', 10, 2);
function handle_price_range_query_var($query, $query_vars)
{
  if (!empty($query_vars['price_range'])) {
    $price_range = explode('|', esc_attr($query_vars['price_range']));

    if (is_array($price_range) && count($price_range) == 2) {
      $query['meta_query']['relation'] = 'AND';
      $query['meta_query'][] = array(
        'key'     => '_price',
        'value'   => reset($price_range), // From price value
        'compare' => '>=',
        'type'    => 'NUMERIC'
      );

      $query['meta_query'][] = array(
        'key'     => '_price',
        'value'   => end($price_range), // To price value
        'compare' => '<=',
        'type'    => 'NUMERIC'
      );

      // $query['orderby'] = 'meta_value_num'; // sort by price
      // $query['order'] = 'ASC'; // In ascending order
    }
  }
  return $query;
}

// add_action( 'wp_enqueue_scripts', 'my_custom_scripts');
// function my_custom_scripts() {
//     wp_enqueue_script( 'checkout_ajax_script', get_stylesheet_directory_uri() . '/js/script.js', ['jquery'], '', true);
// }

remove_action('shutdown', 'wp_ob_end_flush_all', 1);
add_action('shutdown', function () {
  while (@ob_end_flush());
});


add_action('init', 'add_delete_endpoint');
function add_delete_endpoint()
{
  add_rewrite_endpoint('delete-account', EP_ROOT | EP_PAGES);
}
add_filter('query_vars', 'delete_query_vars', 0);
function delete_query_vars($vars)
{
  $vars[] = 'delete-account';
  return $vars;
}
add_filter('woocommerce_account_menu_items', 'add_new_delete_tab');
function add_new_delete_tab($items)
{
  $items['delete-account'] = 'Cancella Account';
  return $items;
}
add_action('woocommerce_account_delete-account_endpoint', 'delete_tab_content');
function delete_tab_content()
{
  echo "<p class='h1 text-secondary mb-4'>Cancella Account</p>";
  echo "<p class='text-text body-medium'>Ci dispiace molto tu voglia abbandonarci! Abbiamo mandato troppe email? Puoi scegliere di cancellare il tuo contatto dalla nostra lista; </p>";
  echo "<div class='mt-3 mb-5'><form method='post'>
            <input type='hidden' name='_mc4wp_action' value='unsubscribe' />
            <button type='submit' class='btn btn-txt btn-light d-block ms-auto me-auto'>ESEGUI</button>
          </form></div>
        ";
  echo "<p class='text-text body-medium'>Se invece vuoi proprio cancellare il tuo account ti chiediamo di prestare attenzione alle seguenti conseguenze; 
    <br><br>
    Assicurati di salvare i dettagli importanti dell'ordine (come le informazioni sulla garanzia) prima di eliminare il tuo account. Potrai sempre contattarci per ricevere assistenza in futuro, ma non avremo accesso a queste informazioni e avremo bisogno che tu le verifichi. Qualsiasi precedente comunicazione con il servizio clienti verrà cancellata. </p>";
  echo do_shortcode('[plugin_delete_me /]');
}


add_filter('woocommerce_checkout_fields', 'custom_checkout_fields');
function custom_checkout_fields($fields)
{
  $fields['billing']['billing_first_name']['label'] = ' ';
  $fields['billing']['billing_first_name']['placeholder'] = 'FIRST NAME';
  $fields['billing']['billing_last_name']['placeholder'] = 'LAST NAME';
  $fields['billing']['billing_last_name']['class'] = 'granpatate2';
  $fields['billing']['billing_company']['placeholder'] = 'COMPANY';
  $fields['billing']['billing_city']['placeholder'] = 'CITY';
  $fields['billing']['billing_phone']['placeholder'] = 'NUMERO DI TELEFONO';
  $fields['shipping']['shipping_first_name']['label'] = ' ';
  $fields['shipping']['shipping_first_name']['placeholder'] = 'FIRST NAME';

  return $fields;
}
?>