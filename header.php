<?php

/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php wp_body_open(); ?>

	<?php do_action('storefront_before_site'); ?>


	<div id="page" class="hfeed site" style="position: relative;">
		<div class="container" id="top-page">
			<?php do_action('storefront_before_header'); ?>

			<header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">



				<a href="http://localhost/Zoema/wordpress/">
					<div class="site-branding">

						<?php
						the_custom_logo();
						if (is_front_page() && is_home()) :
						?>

						<?php
						else :
						?>

						<?php
						endif;
						?>


					</div><!-- .site-branding -->
				</a>
				<div class="social">
					<div class="fb">
						<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Vector-16.png" alt="">

					</div>
					<div class="instagram">
						<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Group-1.png" alt="">
					</div>
				</div>
				<nav id="site-navigation" class="main-navigation">

					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e('Primary Menu', 'Zoema'); ?></button>

					<div class="up-menu">
						<span><img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Mail.png" alt=""> newsletter </span>
						<span><img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Search_Magnifying_Glass.png" alt=""> search</span>
					</div>
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'secondary',
							'menu_id'        => 'main-menu',
							'menu_class'     => 'menu-menu bw-box d-flex align-items-center'
						)
					);
					?>
				</nav>

				<div class="user">
					<a href="http://localhost/Zoema/wordpress/mio-account/">
						<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/User_Square.png" alt="">
					</a>
					<a href="http://localhost/Zoema/wordpress/carrello/" class="cart-link">
						<?php
						$cartnumber =	WC()->cart->get_cart_contents_count();
						if ($cartnumber == 0) { ?>
							<style>
								.cart-count {
									display: inline-block;
								}

								.cart-count-full {
									display: none;
								}
							</style>
							<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/08/Shopping_Cart_02.png" alt="">
							<span class="cart-count"><?php echo $cartnumber ?></span>
						<?php
						} else { ?>
							<style>
								.cart-count {
									display: none;
								}

								.cart-count-full {
									display: inline-block;
								}
							</style>
							<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/cart-full.png'); ?>" alt="">
							<span class="cart-count cart-count-full" id="prova"><?php echo $cartnumber ?></span>
						<?php	}
						?>

					</a>
				</div>

			</header><!-- #masthead -->
		</div>

		<?php
		/**
		 * Functions hooked in to storefront_before_content
		 *
		 * @hooked storefront_header_widget_region - 10
		 * @hooked woocommerce_breadcrumb - 10
		 */
		do_action('storefront_before_content');
		?>

		<div id="content" class="site-content" tabindex="-1">
			<div class="col-full">

				<?php
				do_action('storefront_content_top');
				?>

				<button id="top">
					<a href="#top-page">
						<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/08/Chevron_Up_Duo.png" alt="">
					</a>
				</button>