  $( document ).ready(function() {
    let url = new URL(document.URL);
    let myHash = url.hash;
    let hashes = ['shipping', 'pagamento', 'review'];
    const $btn1 = $("#case1");
    const $btn2 = $("#case2");
    const $btn3 = $("#case3");



    if($($btn1).click(() => {
          location.hash = hashes[0];
          myHash = location.hash;

          const contBilling = document.querySelector(".billing-container");
          contBilling.style.display = "none";

          const contButton = document.querySelector(".button-container");
          contButton.style.display = "none";
          
          const contPayment = document.querySelector(".container-payment");
          contPayment.style.display = "block";

          const contPaymentBt = document.querySelector(".payment-bc");
          contPaymentBt.style.display = "block";

          const timeLinePayment = document.querySelector(".payment");
          timeLinePayment.style.display = "block";

          const timeLineBilling = document.querySelector(".billingg");
          timeLineBilling.style.display = "none";
          nextHash();
        }));

    if($($btn2).click(() => {
          location.hash = hashes[1];
          myHash = location.hash;
          const timeLineBilling = document.querySelector(".billingg");
          timeLineBilling.style.display = "none";
          const timeLinePayment = document.querySelector(".payment");
          timeLinePayment.style.display = "none";
          const contPaymentBt = document.querySelector(".payment-bc");
          contPaymentBt.style.display = "none";
          const contPayment = document.querySelector(".container-payment");
          contPayment.style.display = "none";
          const contReview = document.querySelector(".review-container");
          contReview.style.display = "block";

          const timeLineReview = document.querySelector(".review");
          timeLineReview.style.display = "block";

          const form = $('#form_payment').serializeArray();
          let formBilling = form.splice(0,11);
          let formData = JSON.parse(localStorage.getItem('form'));
          $("#review-email").append(formData[10]['value']);
          console.log(formData);
          
          nextHash();
        }));

    if($($btn3).click(() => {
          location.hash = hashes[2];
          myHash = location.hash;
          nextHash();
        }));

    function nextHash(){
      let finalUrl = '';
      switch(myHash){
        case hashes[0]:
          myHash = hashes[1];
          url.hash = hashes[1];
          final = url.href;
          document.location.href = finalUrl;
          break;
        case hashes[1]:
          myHash = hashes[2];
          url.hash = hashes[2];
          finalUrl = url.href;
          document.location.href = finalUrl;
          break;
        case hashes[2]:
          // myHash = hashes[2];
          // url.hash = hashes[2];
          // finalUrl = url.href;
          // document.location.href = finalUrl;
          break;
        }
    }
    

  }
);