$( document ).ready(function() {
    
  getDataFromForm();
  
  function getDataFromForm(){
      let formPayment = document.querySelector(".form-payment");
      console.log(formPayment);
      let name, surname, company, country, address, home , cap, town, prov, telephone, email;
      let obj = {
        name,
        surname,
        company,
        country,
        address,
        home,
        cap,
        town,
        prov,
        telephone,
        email
      }
      // console.log($("#bw-form-container"));
      const $form = $("#bw-form-container");
      // console.log($form);
      const $inputs = $form.find("input, textarea, select");
      $inputs.each(function(){
        $(this).change(() => {
          changeValueInput(this, obj);
        });
      });
      
      function changeValueInput(inputToChange, object){
        const $form = $("#bw-form-container");
        const $inputs = $form.find("input, textarea, select");
        let values = {};
        $inputs.each(function(){
          let name = $(this).attr('name');
          let value = $(this).val();
          values[name] = value;
          // console.log(name);
          // console.log(value);
          // console.log(values[name]);

          let surname = $(this).attr('surname');
          value = $(this).val();
          values[surname] = value;
          

          let company = $(this).attr('company');
          value = $(this).val();
          values[company] = value;


          let address = $(this).attr('address');
          value = $(this).val();
          values[address] = value;


          let postcode = $(this).attr('postcode');
          value = $(this).val();
          values[postcode] = value;


          let city = $(this).attr('city');
          value = $(this).val();
          values[city] = value;


          let phone = $(this).attr('phone');
          value = $(this).val();
          values[phone] = value;


          let email = $(this).attr('email');
          value = $(this).val();
          values[email] = value;
          console.log(values);
        });
        localStorage.setItem('zoema_form_values', JSON.stringify(values));
      }
  }

  window.onload = function() {
    var locStData = localStorage.getItem("zoema_form_values");
    const obj = JSON.parse(locStData);
    console.log(obj.billing_first_name);
    if (obj !== null) {
      $('#billing_first_name').val(obj.billing_first_name);
      $('#billing_last_name').val(obj.billing_last_name);
      $('#billing_company').val(obj.billing_company);
      $('#billing_address_1').val(obj.billing_address_1);
      $('#billing_address_2').val(obj.billing_address_2);
      $('#billing_postcode').val(obj.billing_postcode);
      $('#billing_city').val(obj.billing_city);
      $('#billing_phone').val(obj.billing_phone);
      $('#billing_email').val(obj.billing_email);
    }
}

});  
