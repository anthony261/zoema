<?php

/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

defined('ABSPATH') || exit;
get_header('shop');

do_action('woocommerce_before_cart'); ?>


	<?php do_action('woocommerce_before_cart_table'); ?>
	<div class="cart-ab">
		<h1>CARRELLO</h1>

		<?php do_action('woocommerce_before_cart_contents'); ?>

		<?php
		foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
			$_product   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
			$product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

			if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
				$product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
		?>
				<div class="cart-products">
					<h2 class="modello">
						<?php
															if (!$product_permalink) {
																echo wp_kses_post(apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;');
															} else {
																echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key));
															}

															do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);

															// Meta data.
															echo wc_get_formatted_cart_item_data($cart_item); // PHPCS: XSS ok.

															// Backorder notification.
															if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
																echo wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>', $product_id));
															}
															?>
						</h2>
					<p class="marca">AUSTRALIAN</p>
					<span class="prezzo">
						<?php
						echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); // PHPCS: XSS ok.
						?>
					</span>
					<span class="cancella">cancella</span>

				</div>

				<div class="cart-price">
					<span>SubTotal</span> <span style="float: right;">
						<?php
						echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); // PHPCS: XSS ok.
						?>
					</span>
					<br>
					<span>Costi di spedizione</span> <span style="float: right;">Calcolati allo step successivo</span>

					<?php do_action('woocommerce_cart_contents'); ?>

					<?php if (wc_coupons_enabled()) { ?>
							<input type="text" name="coupon_code" class="coupon" id="coupon_code" value="" placeholder="CODICE COUPON" style="width: 100%;"/> 
							<button type="submit" class="btn-ab" name="apply_coupon" value="<?php esc_attr_e('Apply coupon', 'woocommerce'); ?>">APPLICA</button>
							<?php do_action('woocommerce_cart_coupon'); ?>
					<?php } ?>
					<span style="float: left;">Totale</span>
					<span style="float: right;">
						<?php
						echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); // PHPCS: XSS ok.
						?>
					</span>
				</div>
		<?php
			}
		}
		?>



				<?php do_action('woocommerce_cart_actions'); ?>

				<?php wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce'); ?>

		<?php do_action('woocommerce_after_cart_contents'); ?>
	</div>
	<?php do_action('woocommerce_after_cart_table'); ?>

<?php do_action('woocommerce_before_cart_collaterals'); ?>

<div class="cart-collaterals">
	<?php
	/**
	 * Cart collaterals hook.
	 *
	 * @hooked woocommerce_cross_sell_display
	 * @hooked woocommerce_cart_totals - 10
	 */
	do_action('woocommerce_cart_collaterals');
	?>
</div>

<?php do_action('woocommerce_after_cart'); ?>
<?php
get_footer('shop');
?>