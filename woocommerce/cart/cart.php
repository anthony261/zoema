<?php

/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

defined('ABSPATH') || exit;
get_header('shop');


do_action('woocommerce_before_cart'); ?>


<div class="container container-timeline mt-5 mb-5">
	<div class="timeline-line first">
		<div class="line check"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/cart-timeline.png'); ?>" alt="">
		<span>CARRELLO</span>
	</div>
	<div class="timeline-line second">
		<div class="line"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/where.png'); ?>" alt="">
		<span>DOVE</span>
	</div>
	<div class="timeline-line third">
		<div class="line"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/credit-card.png'); ?>" alt="">
		<span>COME PAGHI?</span>
	</div>
	<div class="timeline-line fourth">
		<div class="line"></div>
	</div>
	<div class="timeline-item ">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/controlla.png'); ?>" alt="">
		<span>CONTROLLA</span>
	</div>
	<div class="timeline-line fifth">
		<div class="line"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/thankyou.png'); ?>" alt="">
		<span>THANK YOU</span>
	</div>
</div>
<div class="container">
	<div class="user-info">
		<a class="btn-ab" href="http://localhost/Zoema/wordpress/mio-account/" style="margin-bottom: 31px;">ACCEDI AL TUO ACCOUNT</a>

		<p style="margin-bottom: 43px;">Oppure compila i campi;</p>


		<form name="checkout" method="post" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">
			<input type="text" placeholder="LA TUA EMAIL" class="input-checkout">
			<input type="text" placeholder="NOME E COGNOME" class="input-checkout">
			<br>
			<input type="text" placeholder="PASSWORD" class="input-checkout">
			<input type="text" placeholder="CONFERMA PASSWORD" class="input-checkout">
			<p>*compila la password se vuoi creare un account</p>
			<input type="checkbox" style="margin-right: 8px;"> Registra i miei dati e crea un account! <br>
			<input type="checkbox" style="margin-right: 8px;"> Iscrivimi alla newsletter
			<div style="margin-top: 50px; padding-left: 130px">
				<a href="http://localhost/Zoema/wordpress/negozio/" class="back mr-5" style="color: black;">TORNA ALLO SHOP</a>

				oppure


				<button class="btn-ab ml-5">CONTINUA</button>

			</div>
		</form>
	</div>

	<form class="woocommerce-cart-form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
		<?php do_action('woocommerce_before_cart_table'); ?>
		<div class="cart-ab">
			<h1>CARRELLO</h1>
			<?php do_action('woocommerce_before_cart_contents'); ?>

			<?php
			foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
				$_product   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
				$product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

				if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
					$product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
			?>
					<div class="cart-products">
						<h2 class="modello">
							<?php
							if (!$product_permalink) {
								echo wp_kses_post(apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;');
							} else {
								echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key));
							}

							do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);

							// Meta data.
							echo wc_get_formatted_cart_item_data($cart_item); // PHPCS: XSS ok.

							// Backorder notification.
							if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
								echo wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>', $product_id));
							}
							?>
						</h2>
						<p class="marca"></p>
						<span class="prezzo">
							<?php
							echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); // PHPCS: XSS ok.
							?>
						</span>
						<span class="cancella">
							<?php
							echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
								'woocommerce_cart_item_remove_link',
								sprintf(
									'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
									esc_url(wc_get_cart_remove_url($cart_item_key)),
									esc_html__('Remove this item', 'woocommerce'),
									esc_attr($product_id),
									esc_attr($_product->get_sku())
								),
								$cart_item_key
							);
							?>
						</span>
						<div class="cart-price cart-price-cart">
							<span>SubTotal</span> <span style="float: right;">
								<?php
								echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); // PHPCS: XSS ok.
								?>
							</span>
							<br>
							<span>Costi di spedizione</span> <span style="float: right;">Calcolati allo step successivo</span>

							<h1 class="title-coupon">Hai un coupon?</h1>
							<?php do_action('woocommerce_cart_contents'); ?>

							<?php if (wc_coupons_enabled()) { ?>
								<input type="text" name="coupon_code" class="coupon" id="coupon_code" value="" placeholder="CODICE COUPON" style="width: 100%;" />
								<button type="submit" class="btn-ab" name="apply_coupon" value="<?php esc_attr_e('Apply coupon', 'woocommerce'); ?>">APPLICA</button>
								<?php do_action('woocommerce_cart_coupon'); ?>
							<?php } ?>
							<span style="float: left;">Totale</span>
							<span style="float: right;">
								<?php
								echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); // PHPCS: XSS ok.
								?>
							</span>
						</div>
					</div>
			<?php
				}
			}
			?>
		</div>

	</form>

</div>

<?php do_action('woocommerce_before_cart_collaterals'); ?>



<?php do_action('woocommerce_after_cart'); ?>
<?php
get_footer('shop');
?>