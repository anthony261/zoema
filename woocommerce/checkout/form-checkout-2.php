<?php

/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if (!defined('ABSPATH')) {
	exit;
}

get_header('shop');
?>
<div class="container">

	<h2>&nbsp;</h2>



	<div class="row">


		<div class="col-md-4">



			<a onclick="showDataAjax('#Linux');history.pushState({},'','#Linux');">

				<button type="button" class="btn btn-info btn-lg">Linux</button>

			</a>

		</div>



		<div class="col-md-4">

			<a onclick="showDataAjax('#Microsoft');history.pushState({},'','#Microsoft');">

				<button type="button" class="btn btn-info btn-lg">Microsoft</button>

			</a>

		</div>



		<div class="col-md-4">

			<a onclick="showDataAjax('#Apple');history.pushState({},'','#Apple');">

				<button type="button" class="btn btn-info btn-lg">Apple</button>

			</a>

		</div>


	</div>

	<?php 
		echo  $_SERVER['REQUEST_URI'];
		
	?>

	<div class="row">

		<div class="col-md-12" id="show_data" style="background-color: #fff68d;margin-top: 15px;height: 100px;">
			


		</div>

	</div>



	<div class="row">


		<div class="col-md-12">

			<a class="btn btn-info btn-lg" style="margin-top: 15px;" onclick="history.pushState({},'',res[0]);">Refresh</a>

		</div>



	</div>



</div>

<?php
get_footer('shop');
?>