<?php

/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if (!defined('ABSPATH')) {
	exit;
}

get_header('shop');
?>

<div class="container container-checkout d-flex">
	<div class="user-info">
		<div class="btn-ab" style="margin-bottom: 31px;">ACCEDI AL TUO ACCOUNT</div>

		<p style="margin-bottom: 43px;">Oppure compila i campi;</p>


		<form name="checkout" method="post" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">
			<input type="text" placeholder="LA TUA EMAIL" class="input-checkout">
			<input type="text" placeholder="NOME E COGNOME" class="input-checkout">
			<br>
			<input type="text" placeholder="PASSWORD" class="input-checkout">
			<input type="text" placeholder="CONFERMA PASSWORD" class="input-checkout">
			<p>*compila la password se vuoi creare un account</p>
			<input type="checkbox" style="margin-right: 8px;"> Registra i miei dati e crea un account! <br>
			<input type="checkbox" style="margin-right: 8px;"> Iscrivimi alla newsletter
			<div style="margin-top: 50px; padding-left: 130px">
				<a href="<?php echo get_permalink(1) . '/negozio/ ' ?>" class="back mr-5" style="color: black;">TORNA ALLO SHOP</a>

				oppure


				<!-- <button class="btn-ab ml-5">CONTINUA</button> -->
				<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Effettua ordine" data-value="Effettua ordine">Effettua ordine</button>

			</div>
		</form>
	</div>



	<?php do_action('woocommerce_before_cart_table'); ?>
	<div class="cart-ab">
		<h1>CARRELLO</h1>

		<?php do_action('woocommerce_before_cart_contents'); ?>

		<?php
		foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
			$_product   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
			$product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

			if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
				$product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
		?>
				<div class="cart-products">
					<h2 class="modello">
						<?php
						if (!$product_permalink) {
							echo wp_kses_post(apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;');
						} else {
							echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key));
						}

						do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);

						// Meta data.
						echo wc_get_formatted_cart_item_data($cart_item); // PHPCS: XSS ok.

						// Backorder notification.
						if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
							echo wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>', $product_id));
						}
						?>
					</h2>
					<p class="marca"></p>
					<span class="prezzo">
						<?php
						echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); // PHPCS: XSS ok.
						?>
					</span>
					<span class="cancella">
						<?php
						echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
							'woocommerce_cart_item_remove_link',
							sprintf(
								'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
								esc_url(wc_get_cart_remove_url($cart_item_key)),
								esc_html__('Remove this item', 'woocommerce'),
								esc_attr($product_id),
								esc_attr($_product->get_sku())
							),
							$cart_item_key
						);
						?>
					</span>

				</div>

				<div class="cart-price">
					<span>SubTotal</span> <span style="float: right;">
						<?php
						echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); // PHPCS: XSS ok.
						?>
					</span>
					<br>
					<span style="margin-top: 12px; display: inline-block">Costi di spedizione</span> <span style="float: right;">Calcolati allo step successivo</span>

					<h1 class="title-coupon">Hai un coupon?</h1>

					<?php do_action('woocommerce_cart_contents'); ?>

					<?php if (wc_coupons_enabled()) { ?>
						<input type="text" name="coupon_code" class="coupon" id="coupon_code" value="" placeholder="CODICE COUPON" style="width: 100%;" />
						<button type="submit" class="btn-ab" name="apply_coupon" value="<?php esc_attr_e('Apply coupon', 'woocommerce'); ?>">APPLICA</button>
						<?php do_action('woocommerce_cart_coupon'); ?>
					<?php } ?>
					<span style="float: left;">Totale</span>
					<span style="float: right;">
						<?php
						echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); // PHPCS: XSS ok.
						?>
					</span>
				</div>
		<?php
			}
		}
		?>



		<?php do_action('woocommerce_cart_actions'); ?>

		<?php wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce'); ?>

		<?php do_action('woocommerce_after_cart_contents'); ?>
	</div>
	<?php do_action('woocommerce_after_cart_table'); ?>


</div>
<div class="container container-shipping">
	<div class="shipping-title container" style="padding-left: 0 !important; padding-right: 0 !important; margin-bottom: 48px;">
		<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Group-242.png" alt="" style="display: inline-block;">
		SHIPPING
	</div>
	<form name="checkout" method="post" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">
		<input type="text" placeholder="PAESE" class="input-checkout">
		<input type="text" placeholder="PROVINCIA" class="input-checkout">
		<br>
		<input type="text" placeholder="CITTA'" class="input-checkout">
		<input type="text" placeholder="CODICE FISCALE" class="input-checkout">
		<br>
		<input type="text" placeholder="INDIRIZZO" class="input-checkout">
		<input type="text" placeholder="TELEFONO" class="input-checkout">
		<div style="margin-top: 50px; padding-left: 130px">
			<a href="<?php echo get_permalink(1) . '/negozio/ ' ?>" class="back mr-5" style="color: black;">TORNA ALLO SHOP</a>

			oppure


			<!-- <button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Effettua ordine" data-value="Effettua ordine">Effettua ordine</button> -->

		</div>
	</form>
</div>

<div class="container container-pagamento">
	<div class="pagamento-title container" style="padding-left: 0 !important; padding-right: 0 !important; margin-bottom: 48px;">
		<img src="http://localhost/Zoema/wordpress/wp-content/uploads/2022/07/Group-242.png" alt="" style="display: inline-block;">
		PAGAMENTO
	</div>
	<form name="checkout" method="post" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">
		<input type="text" placeholder="NUMERO CARTA" class="input-checkout">
		<input type="text" placeholder="NOME SULLA CARTA" class="input-checkout">
		<br>
		<input type="text" placeholder="DATA DI SCADENZA (MM/AA)" class="input-checkout">
		<input type="text" placeholder="CODICE DI SICUREZZA" class="input-checkout">
		<br>
		<div style="margin-top: 50px; padding-left: 130px">
			<a href="<?php echo get_permalink(1) . '/negozio/ ' ?>" class="back mr-5" style="color: black;">TORNA ALLO SHOP</a>

			oppure


			<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Effettua ordine" data-value="Effettua ordine">Effettua ordine</button>

		</div>

		<div class="container">
			<?php do_action('woocommerce_checkout_before_order_review'); ?>

			<div id="order_review" class="woocommerce-checkout-review-order">
				<?php do_action('woocommerce_checkout_order_review'); ?>
			</div>

			<?php do_action('woocommerce_checkout_after_order_review'); ?>
		</div>
</div>

<?php
get_footer('shop');
?>