<?php

/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */


if (!defined('ABSPATH')) {
	exit;
}

get_header('shop');



do_action('woocommerce_before_checkout_form', $checkout);

// If checkout registration is disabled and not logged in, the user cannot checkout.
if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
	echo esc_html(apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce')));
	return;
}

?>

<div class="container container-timeline billingg mt-5 mb-5">
	<div class="timeline-line first">
		<div class="line check"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/cart-timeline.png'); ?>" alt="">
		<span>CARRELLO</span>
	</div>
	<div class="timeline-line second">
		<div class="line check"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/gps.png'); ?>" alt="">
		<span>DOVE</span>
	</div>
	<div class="timeline-line third">
		<div class="line check"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/credit-card.png'); ?>" alt="">
		<span>COME PAGHI?</span>
	</div>
	<div class="timeline-line fourth">
		<div class="line"></div>
	</div>
	<div class="timeline-item ">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/controlla.png'); ?>" alt="">
		<span>CONTROLLA</span>
	</div>
	<div class="timeline-line fifth">
		<div class="line"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/thankyou.png'); ?>" alt="">
		<span>THANK YOU</span>
	</div>
</div>

<div class="container container-timeline payment mt-5 mb-5">
	<div class="timeline-line first">
		<div class="line check"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/cart-timeline.png'); ?>" alt="">
		<span>CARRELLO</span>
	</div>
	<div class="timeline-line second">
		<div class="line check"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/gps.png'); ?>" alt="">
		<span>DOVE</span>
	</div>
	<div class="timeline-line third">
		<div class="line check"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/credit-card-color.png'); ?>" alt="">
		<span>COME PAGHI?</span>
	</div>
	<div class="timeline-line fourth">
		<div class="line"></div>
	</div>
	<div class="timeline-item ">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/controlla.png'); ?>" alt="">
		<span>CONTROLLA</span>
	</div>
	<div class="timeline-line fifth">
		<div class="line"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/thankyou.png'); ?>" alt="">
		<span>THANK YOU</span>
	</div>
</div>

<div class="container container-timeline review mt-5 mb-5">
	<div class="timeline-line first">
		<div class="line check"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/cart-timeline.png'); ?>" alt="">
		<span>CARRELLO</span>
	</div>
	<div class="timeline-line second">
		<div class="line check"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/gps.png'); ?>" alt="">
		<span>DOVE</span>
	</div>
	<div class="timeline-line third">
		<div class="line check"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/credit-card-color.png'); ?>" alt="">
		<span>COME PAGHI?</span>
	</div>
	<div class="timeline-line fourth">
		<div class="line check"></div>
	</div>
	<div class="timeline-item ">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/cestino.png'); ?>" alt="">
		<span>CONTROLLA</span>
	</div>
	<div class="timeline-line fifth">
		<div class="line"></div>
	</div>
	<div class="timeline-item">
		<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/thankyou.png'); ?>" alt="">
		<span>THANK YOU</span>
	</div>
</div>

	<form name="checkout" method="post" class="checkout woocommerce-checkout form-payment" id="form_payment" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data" id="form-payment">

		<?php if ($checkout->get_checkout_fields()) : ?>

			<?php do_action('woocommerce_checkout_before_customer_details'); ?>

			<div class="container" id="customer_details">

				<div class="col-12 billing-container">
					<div class="title-billing">
						<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/i.png'); ?>" alt="" style="display: inline-block;"> <span class="billing-title">SPEDIZIONE</span>
					</div>
					<?php do_action('woocommerce_checkout_billing'); ?>
				</div>

				<div class="col-12 d-none">
					<?php do_action('woocommerce_checkout_shipping'); ?>
				</div>
			</div>

			<?php do_action('woocommerce_checkout_after_customer_details'); ?>

		<?php endif; ?>

		<?php do_action('woocommerce_checkout_before_order_review_heading'); ?>

		<h3 id="order_review_heading" class="d-none"><?php esc_html_e('Your order', 'woocommerce'); ?></h3>

		<?php do_action('woocommerce_checkout_before_order_review'); ?>

		<div id="order_review" class="woocommerce-checkout-review-order d-none">
			<?php do_action('woocommerce_checkout_order_review'); ?>
		</div>

		<?php do_action('woocommerce_checkout_after_order_review'); ?>

		<div class="container container-payment">
			<div class="title-payment mt-5 mb-5">
				<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/i.png'); ?>" alt="" style="display: inline-block;"> <span class="billing-title">PAGAMENTO</span>
			</div>
			<input type="text" placeholder="NUMERO CARTA" class="input-checkout">
			<input type="text" placeholder="NOME SULLA CARTA" class="input-checkout">
			<br>
			<input type="text" placeholder="DATA DI SCADENZA (MM/AA)" class="input-checkout">
			<input type="text" placeholder="CODICE DI SICUREZZA" class="input-checkout">
			<br>

		</div>

	</form>


	<div class="container review-container">
		<div class="title-review mt-5 mb-5">
			<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/i.png'); ?>" alt="" style="display: inline-block;"> <span class="billing-title">CONTATTI</span>
		</div>

		<p id="review-email"></p>
		<p id="review-name">Anthony Barbagallo</p>
		<p id="review-phone">32033333333</p>

		<div class="title-review mt-5 mb-5">
			<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/i.png'); ?>" alt="" style="display: inline-block;"> <span class="billing-title">SHIPPING</span>
		</div>


		<p id="review-place">via delle Palme 69</p>


		<div class="title-review mt-5 mb-5">
			<img src="<?= esc_url(get_stylesheet_directory_uri() . '/img/i.png'); ?>" alt="" style="display: inline-block;"> <span class="billing-title">PAGAMENTO</span>
		</div>


		<p>carta di credito n* 6444222********</p>

		<div style="margin-top: 50px; padding-left: 130px">
			<a href="<?php //echo get_permalink(1) . '/negozio/ ' 
								?>" class="back mr-5" style="color: black;">TORNA ALLO SHOP</a>

			oppure

			<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Effettua ordine" data-value="Effettua ordine">Effettua ordine</button>
		</div>
	</div>

	<form class="woocommerce-cart-form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
		<?php do_action('woocommerce_before_cart_table'); ?>
		<div class="cart-ab">
			<h1>CARRELLO</h1>
			<?php do_action('woocommerce_before_cart_contents'); ?>

			<?php
			foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
				$_product   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
				$product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

				if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
					$product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
			?>
					<div class="cart-products">
						<h2 class="modello">
							<?php
							if (!$product_permalink) {
								echo wp_kses_post(apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;');
							} else {
								echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key));
							}

							do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);

							// Meta data.
							echo wc_get_formatted_cart_item_data($cart_item); // PHPCS: XSS ok.

							// Backorder notification.
							if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
								echo wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>', $product_id));
							}
							?>
						</h2>
						<p class="marca"></p>
						<span class="prezzo">
							<?php
							echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); // PHPCS: XSS ok.
							?>
						</span>
						<span class="cancella">
							<?php
							echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
								'woocommerce_cart_item_remove_link',
								sprintf(
									'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
									esc_url(wc_get_cart_remove_url($cart_item_key)),
									esc_html__('Remove this item', 'woocommerce'),
									esc_attr($product_id),
									esc_attr($_product->get_sku())
								),
								$cart_item_key
							);
							?>
						</span>
						<div class="cart-price cart-price-cart">
							<span>SubTotal</span> <span style="float: right;">
								<?php
								echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); // PHPCS: XSS ok.
								?>
							</span>
							<br>
							<span>Costi di spedizione</span> <span style="float: right;">Calcolati allo step successivo</span>

							<h1 class="title-coupon">Hai un coupon?</h1>
							<?php do_action('woocommerce_cart_contents'); ?>

							<?php if (wc_coupons_enabled()) { ?>
								<input type="text" name="coupon_code" class="coupon" id="coupon_code" value="" placeholder="CODICE COUPON" style="width: 100%;" />
								<button type="submit" class="btn-ab" name="apply_coupon" value="<?php esc_attr_e('Apply coupon', 'woocommerce'); ?>">APPLICA</button>
								<?php do_action('woocommerce_cart_coupon'); ?>
							<?php } ?>
							<span style="float: left;">Totale</span>
							<span style="float: right;">
								<?php
								echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); // PHPCS: XSS ok.
								?>
							</span>
						</div>
					</div>
			<?php
				}
			}
			?>
		</div>

	</form>


<div class="container button-container mb-5">
	<a href="http://localhost/Zoema/wordpress/negozio/" class="back mr-5" style="color: black;">TORNA ALLO SHOP</a>

	oppure

	<button class="btn-ab ml-5" id="case1">CONTINUA</button>
</div>

<div class="container payment-bc mb-5">
	<a href="http://localhost/Zoema/wordpress/negozio/" class="back mr-5" style="color: black;">TORNA ALLO SHOP</a>

	oppure

	<button class="btn-ab ml-5" id="case2">CONTINUA</button>
</div>







<?php
do_action('woocommerce_after_checkout_form', $checkout); ?>


<?php
get_footer('shop');
?>